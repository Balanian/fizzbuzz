using System;
using System.Collections.Generic;
using fizzbuzz.domain;
using FluentAssertions;
using Xunit;

namespace fizzbuzz.tests
{
    public class fizzbuzTetst
    {
        [Fact]
        public void should_return_fizz_on_number_3()
        {
            var expected = new List<string>() {"1", "2", "fizz"};

            var result = FizzbuzzGenerator.generate(1, 3);

            result.Should().BeEquivalentTo(expected);

        }

        [Fact]
        public void should_return_buzz_on_number_5()
        {
            var expected = new List<string>() {"4","buzz" };

            var result = FizzbuzzGenerator.generate(4, 5);

            result.Should().BeEquivalentTo(expected);

        }

        [Fact]
        public void should_return_fizzbuzz_on_number_15()
        {
            var expected = new List<string>() { "13", "14","fizzbuzz" };

            var result = FizzbuzzGenerator.generate(13, 15);

            result.Should().BeEquivalentTo(expected);

        }
    }

  
}
