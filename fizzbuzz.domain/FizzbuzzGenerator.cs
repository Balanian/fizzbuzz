﻿using System;
using System.Collections.Generic;

namespace fizzbuzz.domain
{
    public class FizzbuzzGenerator
    {
        public static List<string> generate(int start, int end)
        {
            var output = new List<string>();
            for (int i = start; i <= end; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                    output.Add("fizzbuzz");
                else if (i % 3 == 0)
                    output.Add("fizz");
                else if (i % 5 == 0)
                    output.Add("buzz");
                else output.Add(i.ToString());
            }

            return output;
        }
    }
}
